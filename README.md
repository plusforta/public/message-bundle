# Message Bundle

The library defines various commands, events and DTOs that are shared by services,
to ensure their communication.

Two systems that wish to communicate over the symfony messenger system can use these
classes to define the messages to be sent and DTOs for transforming the data.

## Deprecation

We are moving away from Message wherever possible between services.
The tight coupling between services is not a good practice.

## Current Usage

| Project        | Version Used |
|----------------|:------------:|
| reports        |    1.2.4     |
| export-service |    1.2.4     |
| email-service  |    1.2.4     |

As the time of this writing, dev-master the same as version 1.2.2. 

## Installation

```shell
composer require plusforta/message-bundle:dev-master
```
