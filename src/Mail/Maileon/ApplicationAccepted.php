<?php


namespace Plusforta\Messages\Mail\Maileon;


/**
 *
 * @property string pdfLink
 * @property string secureId
 * @property string humanContractName
 * @property string transactionId
 */
class ApplicationAccepted extends BaseTemplateMail
{
    protected int $type = 34;

    protected int $typeKfx = 45;

    protected array $requiredFields = [
        'pdfLink',
        'secureId',
        'humanContractName',
        'transactionId',
    ];

    protected array $optionalFields = [];

    /**
     * @return array
     */
    public function getContent(): array
    {
        return [
            'first_name' => $this->firstName,
            'name' => $this->name,
            'salutation' => $this->salutation,
            'pdf_link' => $this->pdfLink,
            'secure_id' => $this->secureId,
            'human_contract_name' => $this->humanContractName,
            'transaction_id' => $this->transactionId,
        ];
    }

}
