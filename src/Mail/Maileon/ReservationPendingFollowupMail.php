<?php


namespace Plusforta\Messages\Mail\Maileon;


class ReservationPendingFollowupMail extends ReservationPendingMail
{
    protected int $type = 52;

    protected int $typeKfx = 51;

}
