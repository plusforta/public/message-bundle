<?php


namespace Plusforta\Messages\Mail\Maileon;

use Plusforta\Messages\Mail\Maileon\Exceptions\MailException;
use Plusforta\Messages\Mail\MailInterface;
use Webmozart\Assert\Assert;

/**
 * @property string firstName
 * @property string name
 * @property string salutation
 * @property string addressStreet
 * @property string addressStreetNumber
 * @property string addressCity
 * @property string addressZip
 * @property string dateOfBirth
 * @property string title
 * @property string version
 * @property mixed orderId
 * @property mixed product
 * @property bool isAccepted
 * @property bool noAdvertising
 * @property bool sellingKeySafeing
 * @property bool sellingSelfStorage
 * @property bool sellingMovePackage
 */
abstract class BaseTemplateMail implements TemplateMailInterface
{
    protected static ?string $_name = null;
    protected const B2C = '1';

    public function getName(): string
    {
        if (static::$_name === null) {
            $refClass = new \ReflectionClass($this);
            self::$_name = $refClass->getShortName();
        }

        return static::$_name;
    }
    protected const ERROR_MESSAGE_FIELD_REQUIRED = 'The field "%s" is required and cannot be empty';

    protected const ERROR_MESSAGE_NEITHER_REQUIRED_NOR_OPTIONAL = 'Property "%s" is neither required nor optional.';

    protected ?string $sender = null;
    protected ?string $senderFullName = null;

    protected int $type;

    protected int $typeKfx;

    public bool $isKfx = false;

    protected array $requiredFields = [];

    private array $defaultRequiredFields = [
        'firstName',
        'name',
        'salutation',
        'addressStreet',
        'addressStreetNumber',
        'addressCity',
        'addressZip',
        'dateOfBirth',
        'orderId',
        'product',
        'isAccepted',
        'noAdvertising',
    ];

    protected array $optionalFields = [];

    private array $defaultOptionalFields = [
        'isKfx',
        'title',
        'version',
        'sellingKeySafeing',
        'sellingSelfStorage',
        'sellingMovePackage',
    ];

    /**
     * @var string[]
     */
    protected array $data = [];

    protected string $email;

    /**
     * @var string
     */
    protected string $_orderId;

    protected string $_product;


    public function getEmail(): string
    {
        return $this->email;
    }


    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function getSenderFullName(): ?string
    {
        return $this->senderFullName;
    }

    public function setSenderFullName(?string $senderFullName): void
    {
        $this->senderFullName = $senderFullName;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int
     * @throws MailException
     */
    public function getType(): int
    {
        if ($this->isKfx && $this->typeKfx) {
            return $this->typeKfx;
        }

        if (!isset($this->type)) {
            throw new MailException('type has to be set');
        }

        return $this->type;
    }


    /**
     * @param string $name
     * @param string $value
     */
    public function __set(string $name, string $value)
    {
        if (in_array($name, $this->requiredFields)) {
            $this->data[$name] = $value;
        }

        if (in_array($name, $this->optionalFields)) {
            $this->data[$name] = $value;
        }
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        if (in_array($name, $this->requiredFields, true)) {
            if (!isset($this->$name)) {
                throw new \Exception(sprintf(self::ERROR_MESSAGE_FIELD_REQUIRED, $name));
            }

            return $this->data[$name];
        }

        if (in_array($name, $this->optionalFields, true)) {
            return $this->data[$name] ?? '';
        }

        throw new \Exception(sprintf(self::ERROR_MESSAGE_NEITHER_REQUIRED_NOR_OPTIONAL, $name));
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function getParameters(): array
    {
        $email = $this->getEmail();

        return [
                'email' => $email,
                'standard_fields' => $this->getContactStandardFields(),
                'custom_fields' => $this->getContactCustomFields(),
        ];
    }


    private function __construct(string $orderId, string $product)
    {
        $this->requiredFields = array_merge($this->defaultRequiredFields, $this->requiredFields);
        $this->optionalFields = array_merge($this->defaultOptionalFields, $this->optionalFields);
        $this->_orderId = $orderId;
        $this->_product = $product;
    }

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        Assert::notEmpty($orderId);
        Assert::notEmpty($product);
        return new static($orderId, $product);
    }

    private function getContactStandardFields()
    {
        return [
            'FIRSTNAME' => $this->firstName,
            'LASTNAME' => $this->name,
            'SALUTATION' => $this->salutation,
            'ADDRESS' => $this->addressStreet,
            'HNR' => $this->addressStreetNumber,
            'CITY' => $this->addressCity,
            'ZIP' => $this->addressZip,
            'BIRTHDAY' => $this->dateOfBirth,
            'TITLE' => $this->title,
        ];
    }

    private function getContactCustomFields(): array
    {
        return array(
            'b2c' => self::B2C,
            'order_id' => $this->orderId,
            'order_type' => $this->product,
            'is_accepted' => $this->isAccepted ? 1 : 0,
            'no_advertising' => $this->noAdvertising ? 1 : 0,
            'brand_with_category' => 'kf_tenancy_retail',
            'selling_key_safeing' => $this->sellingKeySafeing ? 1 : 0,
            'selling_self_storage' => $this->sellingSelfStorage ? 1 : 0,
            'selling_move_package' => $this->sellingMovePackage ? 1 : 0,
            'version' => $this->version
        );
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        foreach ($this->requiredFields as $requiredField) {
            if (!isset($this->$requiredField)) {
                return false;
            }
        }

        return true;
    }


    /**
     * @return string[]
     */
    public function getRequiredFields(): array
    {
        return $this->requiredFields;
    }

    /**
     * @return string[]
     */
    public function getOptionalFields(): array
    {
        return $this->optionalFields;
    }


    public function getOrderId(): string
    {
        return $this->_orderId;
    }

    public function getProduct(): string
    {
        return $this->_product;
    }


}
