<?php


namespace Plusforta\Messages\Mail\Maileon;


/**
 *
 * @property string transactionId
 * @property string secureId
 * @property string humanContractName
 * @property string pdfLink
 */
class HeysafeFinished extends BaseTemplateMail
{
    protected int $type = 74;

    protected array $requiredFields = [
        'transactionId',
        'secureId',
        'humanContractName',
        'pdfLink',
    ];

    /**
     * @return array
     */
    public function getContent(): array
    {
        return [
            'first_name' => $this->firstName,
            'name' => $this->name,
            'salutation' => $this->salutation,
            'transaction_id' => $this->transactionId,
            'secure_id' => $this->secureId,
            'human_contract_name' => $this->humanContractName,
            'pdf_link' => $this->pdfLink,
        ];
    }

}
