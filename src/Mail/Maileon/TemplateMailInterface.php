<?php

namespace Plusforta\Messages\Mail\Maileon;

use \Plusforta\Messages\Mail\MailInterface;

interface TemplateMailInterface extends MailInterface
{

    public function getType(): int;

    public function getName(): string;

    public function getParameters(): array;

    public function getContent(): array;

}
