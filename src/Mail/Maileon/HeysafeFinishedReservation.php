<?php


namespace Plusforta\Messages\Mail\Maileon;

/**
 *
 * @property string secureId
 * @property string humanContractName
 * @property string pdfLink
 */
class HeysafeFinishedReservation extends BaseTemplateMail
{
    protected int $type = 75;

    protected array $requiredFields = [
        'secureId',
        'humanContractName',
        'pdfLink',
    ];


    /**
     * @return array
     */
    public function getContent(): array
    {
        return [
            'first_name' => $this->firstName,
            'name' => $this->name,
            'salutation' => $this->salutation,
            'secure_id' => $this->secureId,
            'human_contract_name' => $this->humanContractName,
            'pdf_link' => $this->pdfLink,
        ];
    }

}
