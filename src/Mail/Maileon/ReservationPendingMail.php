<?php


namespace Plusforta\Messages\Mail\Maileon;


/**
 * @property mixed secureId
 * @property mixed humanContractName
 * @property mixed transactionId
 * @property mixed pdfLinkReservation
 */
class ReservationPendingMail extends BaseTemplateMail
{
    protected int $type = 38;

    protected int $typeKfx = 47;

    protected array $requiredFields = [
        'pdfLinkReservation',
        'secureId',
        'humanContractName',
        'transactionId',
    ];

    protected array $optionalFields = [];
    /**
     * @return array
     */
    public function getContent(): array
    {
        return [
            'secure_id' => $this->secureId,
            'first_name' => $this->firstName,
            'human_contract_name' => $this->humanContractName,
            'name' => $this->name,
            'pdf_link' => $this->pdfLinkReservation,
            'import' => 'import',
            'salutation' => $this->salutation,
            'transaction_id' => $this->transactionId,

        ];
    }

}
