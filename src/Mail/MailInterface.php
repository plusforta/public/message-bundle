<?php


namespace Plusforta\Messages\Mail;


interface MailInterface
{

    public function getOrderId(): string;

    public function getProduct(): string;

    public function getEmail(): string;

    public function getSender(): ?string;

    public function getSenderFullName(): ?string;

}