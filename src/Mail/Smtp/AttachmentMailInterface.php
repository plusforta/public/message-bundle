<?php

namespace Plusforta\Messages\Mail\Smtp;

use Plusforta\Messages\Mail\MailInterface;

interface AttachmentMailInterface extends MailInterface
{

    public function getSubject(): ?string;

    public function getText(): ?string;

    public function getHtml(): ?string;

    public function getAttachments(): array;

}