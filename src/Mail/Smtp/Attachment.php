<?php

namespace Plusforta\Messages\Mail\Smtp;

use Webmozart\Assert\Assert;

class Attachment
{
    public const CONTENT_TYPE_APPLICATION_PDF = 'application/pdf';
    public const CONTENT_TYPE_APPLICATION_XML = 'application/xml';
    public const CONTENT_TYPE_APPLICATION_ZIP = 'application/zip';

    private string $provider;
    private string $path;
    private string $contentType;
    private ?string $filename = null;

    private function __construct(string $provider, string $path, string $contentType)
    {
        $this->provider = $provider;
        $this->path = $path;
        $this->contentType = $contentType;
    }

    public static function fromProviderWithPathAndContentType(string $provider, string $path, string $contentType): self
    {
        Assert::notEmpty($provider);
        Assert::notEmpty($path);
        Assert::notEmpty($contentType);

        return new self($provider, $path, $contentType);
    }

    public static function pdfFromProviderWithPath(string $provider, string $path): self
    {
        return self::fromProviderWithPathAndContentType($provider, $path, self::CONTENT_TYPE_APPLICATION_PDF);
    }

    public static function xmlFromProviderWithPath(string $provider, string $path): self
    {
        return self::fromProviderWithPathAndContentType($provider, $path, self::CONTENT_TYPE_APPLICATION_XML);
    }

    public function withFileName(string $filename): self
    {
        Assert::notEmpty($filename);
        $this->filename = $filename;
        return $this;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }
}