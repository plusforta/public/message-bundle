<?php

namespace Plusforta\Messages\Mail\Smtp;


use Webmozart\Assert\Assert;

class AttachmentMail implements AttachmentMailInterface
{

    private string $orderId;
    private string $product;
    private string $email;
    private string $sender;
    private ?string $senderFullName = null;
    private ?string $subject = null;
    private ?string $text = null;
    private ?string $html = null;
    private array $attachments = [];

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function withSender(string $sender): self
    {
        $this->sender = $sender;
        return $this;
    }

    public function withText(string $text): self
    {
        Assert::notEmpty($text);
        $this->text = $text;
        return $this;
    }

    public function withHtml(string $html): self
    {
        Assert::notEmpty($html);
        $this->html = $html;
        return $this;
    }

    public function withSubject(string $subject): self
    {
        Assert::notEmpty($subject);
        $this->subject = $subject;
        return $this;
    }

    public function addAttachment(Attachment $attachment): self
    {
        $this->attachments[] = $attachment;
        return $this;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return Attachment[]
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function getSenderFullName(): ?string
    {
        return $this->senderFullName;
    }

    public function setSenderFullName(?string $senderFullName): void
    {
        $this->senderFullName = $senderFullName;
    }

    public function validateMail(string $email): void
    {
        Assert::email($email);
    }
}