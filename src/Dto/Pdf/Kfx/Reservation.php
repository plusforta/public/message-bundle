<?php

namespace Plusforta\Messages\Dto\Pdf\Kfx;

use DateTimeInterface;
use Plusforta\Messages\Dto\Pdf\ReservationDataInterface;

class Reservation implements ReservationDataInterface
{
    public Tenant $tenant;

    public ?DateTimeInterface $issueDate = null;

}