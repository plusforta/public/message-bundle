<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


class Tenant
{
    public ?string $gender = null;

    public ?string $name = null;

    public ?string $firstName = null;

    public ?string $birthDate = null;

    public ?Address $address = null;
}