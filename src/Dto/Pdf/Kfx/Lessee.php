<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


class Lessee
{
    public ?string $gender = null;

    public ?string $name = null;

    public ?string $firstName = null;

    public ?string $phone = null;

    public ?string $email = null;

    public ?string $birthDay = null;

    public ?Address $address = null;

}