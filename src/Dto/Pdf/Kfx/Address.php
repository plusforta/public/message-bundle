<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


class Address
{

    public ?string $street = null;

    public ?string $streetNumber = null;

    public ?string $zip = null;

    public ?string $city = null;
}