<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


use Plusforta\Messages\Dto\Pdf\ApplicationDataInterface;

class Application implements ApplicationDataInterface
{

    public const POSTAL_DESTINATION_LEGAL = 'legal_address';
    public const POSTAL_DESTINATION_LANDLORD = 'landlord_address';
    public const POSTAL_DESTINATION_OBJECT = 'object_address';

    public ?string $partnerID = null;

    public ?string $deposit = null;

    public Lessee $firstLessee;

    public ?Lessee $secondLessee = null;

    public Lessor $lessor;

    public Apartment $apartment;

    public ?string $iban;

    public ?string $annualSubscription;

    public ?PaymentConditions $paymentConditionsForProduct;

    public ?string $postalDestination = null;


}