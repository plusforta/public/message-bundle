<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


class Lessor
{

    public ?string $name = null;

    public ?string $firstName = null;

    public ?string $company = null;

    public ?string $companyAppendix = null;

    public ?Address $address = null;

}