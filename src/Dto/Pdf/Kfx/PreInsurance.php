<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


class PreInsurance
{
    public bool $hasPreInsurance = false;

    public ?string $insurer = null;

    public ?string $insuranceNumber = null;

    public ?string $terminatedBy = null;

    public bool $hasPreDamage = false;

    public ?string $preDamage = null;
}