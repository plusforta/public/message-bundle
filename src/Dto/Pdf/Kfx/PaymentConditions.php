<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


class PaymentConditions
{
    public ?float $sumMin = null;
    public ?float $sumMax = null;
    public ?float $sumFactor = null;
}