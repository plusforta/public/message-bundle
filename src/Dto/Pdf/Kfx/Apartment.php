<?php


namespace Plusforta\Messages\Dto\Pdf\Kfx;


class Apartment
{

    public ?string $sum = null;

    public ?Address $address = null;

    public ?string $isExisting = null;

    public ?string $isTemporary = null;

    public ?string $untilTemporary = null;

    public ?string $rentalDate = null;

    public ?string $moveInDate = null;

    public ?PreInsurance $preInsurance = null;
}