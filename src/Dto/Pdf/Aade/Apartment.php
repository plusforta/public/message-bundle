<?php


namespace Plusforta\Messages\Dto\Pdf\Aade;


class Apartment
{

    public ?string $sum = null;

    public ?string $street = null;

    public ?string $streetNumber = null;

    public ?string $zip = null;

    public ?string $city = null;

    public ?string $isExisting = null;

    public ?string $isTemporary = null;

    public ?string $untilTemporary = null;

    public ?string $rentalDate = null;

}