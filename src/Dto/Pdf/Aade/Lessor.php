<?php


namespace Plusforta\Messages\Dto\Pdf\Aade;


class Lessor
{

    public ?string $name = null;

    public ?string $firstName = null;

    public ?string $company = null;

    public ?string $companyAppendix = null;

    public ?string $street = null;

    public ?string $streetNumber = null;

    public ?string $zip = null;

    public ?string $city = null;

}