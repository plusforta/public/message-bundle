<?php


namespace Plusforta\Messages\Dto\Pdf\Aade;


use Plusforta\Messages\Dto\Pdf\ApplicationDataInterface;

class Application implements ApplicationDataInterface
{
    public const POSTAL_DESTINATION_LEGAL = 'legal_address';
    public const POSTAL_DESTINATION_LANDLORD = 'landlord_address';
    public const POSTAL_DESTINATION_OBJECT = 'object_address';
    public const POSTAL_DESTINATION_DIGITAL = 'digital';

    public ?string $partnerID = null;

    public Lessee $firstLessee;

    public Lessee $secondLessee;

    public Lessor $lessor;

    public Apartment $apartment;

    public ?string $iban;

    public ?string $annualSubscription;

    public ?PaymentConditions $paymentConditionsForProduct;

    public bool $onFirstDemand = false;

    public ?string $postalDestination = null;

}