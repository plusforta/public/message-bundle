<?php


namespace Plusforta\Messages\Dto\Pdf\Aade;


class Lessee
{
    public ?string $gender = null;

    public ?string $name = null;

    public ?string $firstName = null;

    public ?string $street = null;

    public ?string $streetNumber = null;

    public ?string $zip = null;

    public ?string $city = null;

    public ?string $phone = null;

    public ?string $email = null;

    public ?string $birthDay = null;


}