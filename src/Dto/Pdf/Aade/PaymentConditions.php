<?php


namespace Plusforta\Messages\Dto\Pdf\Aade;


class PaymentConditions
{
    public ?string $sumMin = null;
    public ?string $sumFactor = null;
}