<?php


namespace Plusforta\Messages\Dto\Schufa;


use Plusforta\Messages\Dto\ResponseInterface;

interface CreditCheckResultInterface extends ResponseInterface
{

}