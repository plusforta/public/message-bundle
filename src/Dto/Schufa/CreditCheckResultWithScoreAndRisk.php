<?php


namespace Plusforta\Messages\Dto\Schufa;


class CreditCheckResultWithScoreAndRisk implements CreditCheckResultInterface
{
    private string $response;
    private float $score;
    private float $risk;
    private string $schufaId;

    private function __construct(string $response, float $score, float $risk)
    {
        $this->response = $response;
        $this->score = $score;
        $this->risk = $risk;
    }

    public static function fromResponseWithScoreAndRisk(string $response, ?float $score, ?float $risk): self
    {
        return new self($response, $score, $risk);
    }

    public function withSchufaId(string $transactionId): self
    {
        $this->schufaId = $transactionId;
        return $this;
    }

    public function getSchufaId(): string
    {
        return $this->schufaId;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function getScore(): float
    {
        return $this->score;
    }

    public function getRisk(): float
    {
        return $this->risk;
    }

}