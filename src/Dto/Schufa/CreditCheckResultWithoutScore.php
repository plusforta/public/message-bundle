<?php


namespace Plusforta\Messages\Dto\Schufa;


use Webmozart\Assert\Assert;

class CreditCheckResultWithoutScore implements CreditCheckResultInterface
{
    private string $response;

    public function __construct(string $response)
    {
        $this->response = $response;
    }

    public static function fromResponse(string $response): self
    {
        Assert::notEmpty($response);
        return new self($response);
    }

    public function getResponse(): string
    {
        return $this->response;
    }


}