<?php


namespace Plusforta\Messages\Dto\Schufa;


class TenantData implements TenantDataInterface
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';
    public const DATE_OF_BIRTH_FORMAT = 'd.m.Y';

    public ?string $title = null;
    public string $firstName;
    public string $name;
    public string $gender;
    public \DateTimeInterface $dateOfBirth;
    public ?string $placeOfBirth = null;
    public Address $currentAddress;
    public ?Address $lastAddress;
}