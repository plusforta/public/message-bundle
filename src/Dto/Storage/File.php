<?php


namespace Plusforta\Messages\Dto\Storage;

use Webmozart\Assert\Assert;

class File
{
    public const FILETYPE_PDF = 'pdf';
    public const FILETYPE_XML = 'xml';
    public const FILETYPE_TXT = 'txt';
    public const FILETYPE_ZIP = 'zip';

    private const ALLOWED_FILETYPES = [
        self::FILETYPE_PDF,
        self::FILETYPE_XML,
        self::FILETYPE_TXT,
        self::FILETYPE_ZIP,
    ];

    private string $provider;
    private string $path;
    private string $filetype;
    private ?string $documentType = null;

    public function __construct(string $provider, string $path, ?string $filetype)
    {
        $this->provider = $provider;
        $this->path = $path;

        if ($filetype !== null) {
            $this->withFileType($filetype);
        }
    }

    public static function fromProviderAndFilePath(string $provider, string $path): self
    {
        Assert::notEmpty($provider);
        Assert::notEmpty($path);

        return new self($provider, $path, null);
    }

    public static function createPdfFromProviderAndFilePath(string $provider, string $path): self
    {
        Assert::notEmpty($provider);
        Assert::notEmpty($path);

        return new self($provider, $path, self::FILETYPE_PDF);
    }

    public static function createXmlFromProviderAndFilePath(string $provider, string $path): self
    {
        Assert::notEmpty($provider);
        Assert::notEmpty($path);

        return new self($provider, $path, self::FILETYPE_XML);
    }

    public static function createTxtFromProviderAndFilePath(string $provider, string $path): self
    {
        Assert::notEmpty($provider);
        Assert::notEmpty($path);

        return new self($provider, $path, self::FILETYPE_TXT);
    }

    public static function createZipFromProviderAndFilePath(string $provider, string $path): self
    {
        Assert::notEmpty($provider);
        Assert::notEmpty($path);

        return new self($provider, $path, self::FILETYPE_ZIP);
    }

    public function withFileType(string $filetype): self
    {
        Assert::oneOf($filetype, self::ALLOWED_FILETYPES);

        $this->filetype = $filetype;
        return $this;
    }

    public function withDocumentType(string $documentType)
    {
        $this->documentType = $documentType;
        return $this;
    }

    public function getDocumentType(): ?string
    {
        return $this->documentType;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getFiletype(): string
    {
        return $this->filetype;
    }

    public function isPdf(): bool
    {
        return $this->filetype === self::FILETYPE_PDF;
    }

    public function isXml(): bool
    {
        return $this->filetype === self::FILETYPE_XML;
    }

    public function isTxt(): bool
    {
        return $this->filetype === self::FILETYPE_TXT;
    }

    public function isZip(): bool
    {
        return $this->filetype === self::FILETYPE_ZIP;
    }

}