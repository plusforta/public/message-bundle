<?php


namespace Plusforta\Messages\Dto\Wbv;

class ApplicationData implements ApplicationDataInterface
{
    /**
     * @var array|TenantData[]
     */
    public array $tenants;

}