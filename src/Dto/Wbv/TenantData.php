<?php


namespace Plusforta\Messages\Dto\Wbv;


class TenantData
{
    public ?string $title = null;
    public string $firstName;
    public string $name;
    public string $gender;
    public string $dateOfBirth;
    public ?string $placeOfBirth = null;
    public AddressData $currentAddress;
    public ?AddressData $lastAddress;
}