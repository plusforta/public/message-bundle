<?php


namespace Plusforta\Messages\Dto\Wbv;

class ReservationData implements ReservationDataInterface
{
    /**
     * @var array|TenantData[]
     */
    public array $tenants;
}