<?php


namespace Plusforta\Messages\Dto\Wbv;

class AddressData
{
    public string $steet;
    public string $steetNumber;
    public string $zip;
    public string $city;
}