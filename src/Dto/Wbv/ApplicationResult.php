<?php


namespace Plusforta\Messages\Dto\Wbv;


class ApplicationResult
{
    private const RESULT_APPLICATION_ACCPECTED = 'application_accepted';
    private const RESULT_APPLICATION_DECLINED = 'application_declined';

    private string $result;

    public function __construct(string $result)
    {
        $this->result = $result;
    }


    public static function accepted(): self
    {
        return new self(self::RESULT_APPLICATION_ACCPECTED);
    }

    public static function declined(): self
    {
        return new self(self::RESULT_APPLICATION_DECLINED);
    }

    public function isAccepted(): bool
    {
        return $this->result === self::RESULT_APPLICATION_ACCPECTED;
    }

    public function isDeclined(): bool
    {
        return $this->result === self::RESULT_APPLICATION_DECLINED;
    }

}