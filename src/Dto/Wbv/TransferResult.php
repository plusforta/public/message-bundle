<?php


namespace Plusforta\Messages\Dto\Wbv;


class TransferResult
{
    private const RESULT_ACCPECTED = 'accepted';
    private const RESULT_DECLINED = 'declined';

    private string $result;

    public function __construct(string $result)
    {
        $this->result = $result;
    }


    public static function accepted(): self
    {
        return new self(self::RESULT_ACCPECTED);
    }

    public static function declined(): self
    {
        return new self(self::RESULT_DECLINED);
    }

    public function isAccepted(): bool
    {
        return $this->result === self::RESULT_ACCPECTED;
    }

    public function isDeclined(): bool
    {
        return $this->result === self::RESULT_DECLINED;
    }

}