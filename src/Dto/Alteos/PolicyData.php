<?php


namespace Plusforta\Messages\Dto\Alteos;


class PolicyData
{
    public const DESTINATION_ADDRESS_CURRENT = 'current';
    public const DESTINATION_ADDRESS_FUTURE = 'future';
    public const DESTINATION_ADDRESS_LANDLORD = 'landlord';
    public const DESTINATION_ADDRESS_DIGITAL = 'digital';

    public ?CustomerData $customer = null;

    public ?RentalObjectData $rentalObject = null;

    public ?LandlordData $landlord = null;

    public ?CreditCheckData $creditCheck = null;

    public ?PaymentData $payment = null;

    public ?string $policyDestinationAddress = null;
}