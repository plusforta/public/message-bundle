<?php


namespace Plusforta\Messages\Dto\Alteos;


class CustomerData
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';

    public string $gender;

    public string $firstName;

    public string $lastName;

    public string $email;

    public ?string $phone = null;

    public \DateTimeInterface $dateOfBirth;

    public Address $address;
}