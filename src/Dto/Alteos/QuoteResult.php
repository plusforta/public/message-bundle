<?php


namespace Plusforta\Messages\Dto\Alteos;


class QuoteResult implements QuoteResultInterface
{
    private float $gross;
    private float $premium;
    private float $taxes;

    public function __construct(float $goss, float $premium, float $taxes)
    {
        $this->gross = $goss;
        $this->premium = $premium;
        $this->taxes = $taxes;
    }


    public static function fromGrossAndPremiumWithTaxes(float $gross, float $premium, float $taxes): self
    {
        return new self($gross, $premium, $taxes);
    }

    public function getGross(): float
    {
        return $this->gross;
    }

    public function getPremium(): float
    {
        return $this->premium;
    }

    public function getTaxes(): float
    {
        return $this->taxes;
    }

}