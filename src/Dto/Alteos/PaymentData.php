<?php


namespace Plusforta\Messages\Dto\Alteos;


class PaymentData
{

    public ?string $firstName = null;
    public ?string $lastName = null;
    public ?string $iban = null;

}