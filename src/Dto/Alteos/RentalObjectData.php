<?php


namespace Plusforta\Messages\Dto\Alteos;


class RentalObjectData
{
    public ?float $deposit = null;
    public ?float $rent = null;
    public ?\DateTimeInterface $rentalDate = null;
    public ?Address $address = null;
}