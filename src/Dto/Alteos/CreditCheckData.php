<?php


namespace Plusforta\Messages\Dto\Alteos;


class CreditCheckData
{
    public ?string $score = null;
    public ?string $scoreType = null;
    public ?string $transactionId = null;
    public ?string $identCheckCode = null;
}