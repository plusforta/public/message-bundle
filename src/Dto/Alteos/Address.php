<?php


namespace Plusforta\Messages\Dto\Alteos;


class Address
{
    public ?string $street = null;
    public ?string $houseNumber = null;
    public ?string $zip = null;
    public ?string $city = null;
}