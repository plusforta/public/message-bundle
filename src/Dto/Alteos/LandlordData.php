<?php


namespace Plusforta\Messages\Dto\Alteos;


class LandlordData
{

    public ?string $firstName = null;
    public ?string $lastName = null;
    public ?string $email = null;
    public ?Address $address = null;

}