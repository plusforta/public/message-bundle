<?php

namespace Plusforta\Messages\Dto\Alteos;

class QuoteData
{
    private float $deposit;
    private float $rent;
    private int $score;
    private array $features = [];

    private function __construct(float $deposit, float $rent, int $score)
    {
        $this->deposit = $deposit;
        $this->rent = $rent;
        $this->score = $score;
    }

    public static function fromDepositAndRentWithScore(float $deposit, float $rent, int $score): self
    {
        return new self($deposit, $rent, $score);
    }

    public function withPersonIdentFeatures(array $features): self
    {
        $this->features = $features;
        return $this;
    }

    public function getDeposit(): float
    {
        return $this->deposit;
    }

    public function getRent(): float
    {
        return $this->rent;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function getFeatures(): array
    {
        return $this->features;
    }

}