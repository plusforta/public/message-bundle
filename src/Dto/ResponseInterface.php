<?php

namespace Plusforta\Messages\Dto;

interface ResponseInterface
{
    public function getResponse(): string;
}