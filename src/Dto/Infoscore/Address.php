<?php


namespace Plusforta\Messages\Dto\Infoscore;


class Address
{
    public const COUNTRY_GERMANY = 'DEU';

    public string $steet;
    public string $steetNumber;
    public string $zip;
    public string $city;
    public string $country;
}