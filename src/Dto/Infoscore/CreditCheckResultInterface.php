<?php


namespace Plusforta\Messages\Dto\Infoscore;


use Plusforta\Messages\Dto\ResponseInterface;

interface CreditCheckResultInterface extends ResponseInterface
{

}