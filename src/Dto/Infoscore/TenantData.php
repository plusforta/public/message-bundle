<?php


namespace Plusforta\Messages\Dto\Infoscore;


class TenantData implements TenantDataInterface
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';

    public ?string $title = null;
    public string $firstName;
    public string $name;
    public string $gender;
    public ?\DateTimeInterface $dateOfBirth = null;
    public ?string $placeOfBirth = null;
    public Address $currentAddress;
    public ?Address $lastAddress;
}