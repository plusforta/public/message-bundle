<?php


namespace Plusforta\Messages\Dto\Infoscore;


class CreditCheckResultWithScore implements CreditCheckResultInterface
{
    private string $response;
    private float $score;
    private array $features = [];
    private string $type;
    private string $transactionId;

    private function __construct(string $response, float $score, string $type)
    {
        $this->response = $response;
        $this->score = $score;
        $this->type = $type;
    }

    public static function fromResponseWithScoreAndType(string $response, float $score, string $type): self
    {
        return new self($response, $score, $type);
    }

    public function withTransactionId(string $transactionId): self
    {
        $this->transactionId = $transactionId;
        return $this;
    }

    public function withPersonIdentFeatures(array $features): self
    {
        $this->features = $features;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function getScore(): float
    {
        return $this->score;
    }

    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    public function getFeatures(): array
    {
        return $this->features;
    }

}