<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Kautionsantrag implements ExportDataInterface
{

    public Systemdaten $Systemdaten;

    public Antrag $Antrag;
}
