<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Mieter
{

    public const GENDER_FEMALE = 'f';
    public const GENDER_MALE = 'm';

    public ?string $Anrede = null;

    public ?string $Name = null;

    public ?string $Vorname = null;

    public ?string $Titel = null;

    public ?string $Strasse = null;

    public ?string $Hausnummer = null;

    public ?string $PLZ = null;

    public ?string $Stadt = null;

    public ?string $Telefonnummer = null;

    public ?string $email = null;

    public ?string $GeburtDatum = null;

    public ?string $GeburtOrt = null;

    public ?string $Zustimmung = null;
}
