<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Kontodaten
{

    public const ZAHLUNGSWEISE_ANNUALLY = 1;
    public const ZAHLUNGSWEISE_SEMI_ANNUALLY = 2;
    public const ZAHLUNGSWEISE_QUARTERLY = 4;
    public const ZAHLUNGSWEISE_MONTHLY = 12;


    public ?string $Inhaber_Vorname = null;

    public ?string $Inhaber_Nachname = null;

    public ?string $Bankname = null;

    public ?string $IBAN = null;

    public ?string $Kontonummer = null;

    public ?string $Bankleitzahl = null;

    public ?string $Zahlungsweise = null;
}
