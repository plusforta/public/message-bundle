<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Bestaetigung
{

    public ?string $Beratungsprotokoll = null;

    public ?string $Bedingungen = null;

    public ?string $Unterschrift = null;
}
