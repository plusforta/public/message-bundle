<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Antrag
{

    public ?string $Product = null;

    public ?string $PartnerID = null;

    public ?string $CustomId = null;

    public ?string $SubPartnerId = null;

    public ?string $Versicherungsnummer = null;

    public ?string $Typ = null;

    public ?Mieter $Mieter1 = null;

    public ?Mieter $Mieter2 = null;

    public ?Vermieter $Vermieter = null;

    public ?Mietobjekt $Mietobjekt = null;

    public ?Kontodaten $Kontodaten = null;

    public ?array $stelleAntragAntwort = null;

    public ?array $gibAntragsstatusAntwort = null;

    public ?array $pruefeBonitaetAntwort = null;

    public ?array $schufa = null;

    public Bestaetigung $Bestaetigung;

    public ?Postbestand $Postbestand = null;

    public ?GlobalExternal $GlobalExternal = null;
}
