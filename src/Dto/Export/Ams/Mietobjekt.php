<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Mietobjekt
{

    public const VERWENDUNG_NEU = 'neu';
    public const VERWENDUNG_BESTAND = 'bestehend';

    public const EMPFAENGER_LEGAL_ADDRESS = 0;
    public const EMPFAENGER_OBJECT_ADDRESS = 1;
    public const EMPFAENGER_LANDLORD_ADDRESS = 2;
    public const EMPFAENGER_DIGITAL = 3;

    public ?string $Verwendung = null;

    public ?string $Strasse = null;

    public ?string $Hausnummer = null;

    public ?string $PLZ = null;

    public ?string $Ort = null;

    public ?string $MietvertragDatum = null;

    public ?string $EinzugDatum = null;

    public ?string $Kautionssumme = null;

    public ?string $Kaltmiete = null;

    public ?string $Praemie = null;

    public ?string $Befristet = null;

    public ?string $Buergschaftsempfaenger = null;

    public ?string $BestaetigungRV = null;

    public ?string $MietvertragBis = null;
}
