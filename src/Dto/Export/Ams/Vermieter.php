<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Vermieter
{

    public const GENDER_FEMALE = 'f';
    public const GENDER_MALE = 'm';
    public const GENDER_COMPANY = 'n';

    public ?string $Anrede = null;

    public ?string $Name = null;

    public ?string $Vorname = null;

    public ?string $Firma = null;

    public ?string $Email = null;

    public ?string $FirmaZusatz = null;

    public ?string $Postfach = null;

    public ?string $Strasse = null;

    public ?string $Hausnummer = null;

    public ?string $PLZ = null;

    public ?string $Stadt = null;

    public ?string $Bezeichnung = null;
}
