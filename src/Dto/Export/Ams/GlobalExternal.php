<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class GlobalExternal
{
    public ?string $HasPreinsurance = null;
    public ?string $HasPredamage = null;
    public ?string $Vorversicherung = null;
    public ?string $SellingMailForwarding = null;
    public ?string $SellingMailForwardingContract = null;
    public ?string $SellingLeadTransfer = null;
    public ?string $SellingSelfStorage = null;
    public ?string $SellingMovePackage = null;
    public ?string $SellingPreisboerse = null;
    public ?string $SellingPreisboerseTime = null;
    public ?string $SellingKeyFinder = null;
    public ?string $SellingWechselpilot = null;

}
