<?php


namespace Plusforta\Messages\Dto\Export\Ams;

class Systemdaten
{

    public const SPARTE_KAUTPF = 'KAUTPF';
    public const VU_KFX = 'WBV-A';
    public const VU_KFDE = 'R+V-A';
    public const VU_AXA = 'AXA-A';

    public ?string $ExportiertAm = null;

    public ?string $AbgezeichnetAm = null;

    public ?string $Bearbeiter = null;

    public ?string $TrackingID = null;

    public ?string $SourceIP = null;

    public ?string $Sparte = null;

    public ?string $VU = null;
}
