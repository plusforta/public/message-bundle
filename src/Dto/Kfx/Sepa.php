<?php


namespace Plusforta\Messages\Dto\Kfx;


class Sepa
{
    public ?string $owner = null;
    public ?string $iban = null;
    public ?string $bankName = null;

}