<?php


namespace Plusforta\Messages\Dto\Kfx;


class RentalObject
{
    public ?Address $address = null;
    public ?RentalContract $rentalContract = null;
    public ?Insurance $insurance = null;
}