<?php


namespace Plusforta\Messages\Dto\Kfx;


class Address
{
    public ?string $street = null;
    public ?string $streetNumber = null;
    public ?string $city = null;
    public ?string $zip = null;
}