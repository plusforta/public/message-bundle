<?php


namespace Plusforta\Messages\Dto\Kfx;


class Tenant
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';

    public ?string $gender = null;
    public ?string $firstName = null;
    public ?string $name = null;
    public ?\DateTimeInterface $dateOfBirth = null;
    public ?string $email = null;
    public ?string $phoneNumber = null;
    public ?Address $address = null;

}