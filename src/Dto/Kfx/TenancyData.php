<?php


namespace Plusforta\Messages\Dto\Kfx;

class TenancyData
{

    public ?string $orderId = null;
    public ?string $product = null;
    public ?string $partnerCode = null;

    public ?Tenant $firstTenant = null;
    public ?Tenant $secondTenant = null;

    public ?Landlord $landlord = null;
    public ?RentalObject $rentalObject = null;
    public ?Payment $payment = null;

}