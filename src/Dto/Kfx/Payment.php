<?php


namespace Plusforta\Messages\Dto\Kfx;


class Payment
{
    public ?string $type = null;
    public ?string $frequency = null;
    public ?Sepa $sepa = null;
}