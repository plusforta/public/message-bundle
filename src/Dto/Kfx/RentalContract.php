<?php


namespace Plusforta\Messages\Dto\Kfx;


class RentalContract
{

    public ?float $deposit = null;
    public ?float $rent = null;
    public ?\DateTimeInterface $signedAt = null;
    public ?\DateTimeInterface $movedInAt = null;
    public ?bool $isLimited = null;
    public ?\DateTimeInterface $limitedUntil = null;

}