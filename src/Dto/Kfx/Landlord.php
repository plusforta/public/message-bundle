<?php


namespace Plusforta\Messages\Dto\Kfx;


class Landlord
{
    public const GENDER_FEMALE = 'female';
    public const GENDER_MALE = 'male';
    public const GENDER_COMPANY = 'company';

    public ?string $gender = null;
    public ?string $firstName = null;
    public ?string $name = null;
    public ?string $companyName = null;
    public ?string $companyNameAppendix = null;
    public ?Address $address = null;

}