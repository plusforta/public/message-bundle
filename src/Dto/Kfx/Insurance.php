<?php


namespace Plusforta\Messages\Dto\Kfx;


class Insurance
{

    public ?bool $hasPreinsurance = null;
    public ?string $insurer = null;
    public ?string $insuranceNumber = null;
    public ?string $terminatedBy = null;
    public ?bool $hasPredamage = null;
    public ?string $preDamage = null;

}