<?php


namespace Plusforta\Messages\Message\Kfx;


interface SubmittedEventInterface
{
    public function getOrderId(): string;
    public function getProduct(): string;

}