<?php


namespace Plusforta\Messages\Message\Kfx;

use Plusforta\Messages\Dto\Kfx\TenancyData;

class ApplicationSubmittedEvent implements SubmittedEventInterface
{

    private string $orderId;
    private string $product;
    private TenancyData $tenancyData;

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }


    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withTenancyData(TenancyData $tenancyData): self
    {
        $this->tenancyData = $tenancyData;
        return $this;
    }

    public function getTenancyData(): TenancyData
    {
        return $this->tenancyData;
    }

}