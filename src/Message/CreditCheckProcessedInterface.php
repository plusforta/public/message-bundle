<?php


namespace Plusforta\Messages\Message;


use Plusforta\Messages\Dto\ResponseInterface;

interface CreditCheckProcessedInterface
{

    /**
     * @return array|ResponseInterface[]
     */
    public function getResults(): array;
}