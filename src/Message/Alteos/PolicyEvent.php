<?php


namespace Plusforta\Messages\Message\Alteos;


abstract class PolicyEvent
{
    private string $orderId;
    private string $product;
    private string $policyId;

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }


    public static function fromOrderIdAndProduct(string $orderId, string $product): PolicyEvent
    {
        return new static($orderId, $product);
    }

    public function withPolicyId(string $policyId): PolicyEvent
    {
        $this->policyId = $policyId;
        return $this;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function getPolicyId(): string
    {
        return $this->policyId;
    }

}