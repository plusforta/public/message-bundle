<?php


namespace Plusforta\Messages\Message\Alteos;


use Plusforta\Messages\Dto\Alteos\QuoteResultInterface;
use Webmozart\Assert\Assert;

class QuoteRequestProcessedEvent
{
    private string $orderId;
    private string $product;
    private string $identifier;
    private QuoteResultInterface $result;

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }


    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withIdentifier(string $identifier): self
    {
        Assert::notEmpty($identifier);

        $this->identifier = $identifier;
        return $this;
    }

    public function withResult(QuoteResultInterface $result): self
    {
        $this->result = $result;
        return $this;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function getResult(): QuoteResultInterface
    {
        return $this->result;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }
}