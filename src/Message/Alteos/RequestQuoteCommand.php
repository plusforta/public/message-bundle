<?php

namespace Plusforta\Messages\Message\Alteos;

use Plusforta\Messages\Dto\Alteos\QuoteData;

class RequestQuoteCommand
{
    private string $orderId;
    private string $product;
    private string $identifier;
    private QuoteData $quoteData;


    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function withQuoteData(QuoteData $quoteData): self
    {
        $this->quoteData = $quoteData;
        return $this;
    }

    public function getQuoteData(): QuoteData
    {
        return $this->quoteData;
    }


}