<?php

namespace Plusforta\Messages\Message\Healthcheck;

class HealthcheckPingEvent
{
    private $serviceName;
    private $timestamp;

    private function __construct(string $serviceName, \DateTimeInterface $timestamp)
    {
        $this->serviceName = $serviceName;
        $this->timestamp = $timestamp;
    }

    public static function fromServiceName(string $name): self
    {
        return new self($name, new \DateTimeImmutable());
    }

    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    public function getTimestamp(): \DateTimeInterface
    {
        return $this->timestamp;
    }

}