<?php


namespace Plusforta\Messages\Message\Schufa;

use Plusforta\Messages\Dto\Schufa\TenantData;

class CheckCreditCommand
{
    private string $orderId;
    private string $product;
    private string $identifier;
    private array $tenantData = [];

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }

    /**
     * @return TenantData[]|array
     */
    public function getTenantData(): array
    {
        return $this->tenantData;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function addTenantData(TenantData $customerData): self
    {
        $this->tenantData[] = $customerData;
        return $this;
    }

    public function withIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;
        return $this;
    }
}