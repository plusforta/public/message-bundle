<?php


namespace Plusforta\Messages\Message\Schufa;


use Plusforta\Messages\Dto\Schufa\CreditCheckResultInterface;
use Plusforta\Messages\Message\CreditCheckProcessedInterface;
use Webmozart\Assert\Assert;

class CreditCheckProcessedEvent implements CreditCheckProcessedInterface
{

    private string $orderId;
    private string $product;
    private string $identifier;
    private array $results = [];

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }


    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withIdentifier(string $identifier): self
    {
        Assert::notEmpty($identifier);

        $this->identifier = $identifier;
        return $this;
    }

    public function addResult(CreditCheckResultInterface $result): self
    {
        $this->results[] = $result;
        return $this;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function getResults(): array
    {
        return $this->results;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }


}