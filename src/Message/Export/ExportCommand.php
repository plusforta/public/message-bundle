<?php

namespace Plusforta\Messages\Message\Export;

use Plusforta\Messages\Dto\Export\Ams\ExportDataInterface;

class ExportCommand
{
    public const EXPORT_APPLICATION = 'export_application';
    public const EXPORT_RESERVATION = 'export_reservation';

    private string $orderId;
    private string $product;
    private string $exportType;
    private ExportDataInterface $data;

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function __construct(string $orderId, string $product, string $exportType)
    {
        $this->orderId = $orderId;
        $this->product = $product;
        $this->exportType = $exportType;
    }

    public static function exportApplicationForOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product, self::EXPORT_APPLICATION);
    }

    public static function exportReservationForOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product, self::EXPORT_RESERVATION);
    }

    public function withExportData(ExportDataInterface $data): self
    {
        $this->data = $data;
        return $this;
    }

    public function getData(): ExportDataInterface
    {
        return $this->data;
    }

    public function isReservation(): bool
    {
        return $this->exportType === self::EXPORT_RESERVATION;
    }

    public function isApplication(): bool
    {
        return $this->exportType === self::EXPORT_APPLICATION;
    }

}