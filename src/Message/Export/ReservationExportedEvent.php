<?php


namespace Plusforta\Messages\Message\Export;


use Plusforta\Messages\Dto\Storage\File;

class ReservationExportedEvent implements TenancyExportedEventInterface
{
    private string $orderId;
    private string $product;
    private array $files = [];

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function addFile(File $file): self
    {
        $this->files[] = $file;
        return $this;
    }

    /**
     * @return File[]
     */
    public function getFiles(): array
    {
        return $this->files;
    }


}