<?php


namespace Plusforta\Messages\Message\Export;

use \Plusforta\Messages\Message\IdentifiableInterface;

interface TenancyExportedEventInterface extends IdentifiableInterface
{

    public function getFiles(): array;

}