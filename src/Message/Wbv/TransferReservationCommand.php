<?php


namespace Plusforta\Messages\Message\Wbv;

use Plusforta\Messages\Dto\Wbv\ReservationDataInterface;

class TransferReservationCommand implements TransferCommandInterface
{

    private string $orderId;
    private string $product;
    private ReservationDataInterface $tenancyData;

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }

    public function getTenancyData(): ReservationDataInterface
    {
        return $this->tenancyData;
    }

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withData(ReservationDataInterface $tenancyData): self
    {
        $this->tenancyData = $tenancyData;
        return $this;
    }

}