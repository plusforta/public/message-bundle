<?php


namespace Plusforta\Messages\Message\Wbv;


use Plusforta\Messages\Dto\Wbv\TransferResult;

class ApplicationProcessedEvent  implements TransferProcessedEvent
{

    private string $orderId;
    private string $product;
    private TransferResult $result;

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }


    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withResult(TransferResult $result): self
    {
        $this->result = $result;
        return $this;
    }

    public function getResult(): TransferResult
    {
        return $this->result;
    }

}