<?php


namespace Plusforta\Messages\Message\Wbv;

use Plusforta\Messages\Dto\Wbv\ApplicationDataInterface;

class TransferApplicationCommand implements TransferCommandInterface
{

    private string $orderId;
    private string $product;
    private ApplicationDataInterface $tenancyData;

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }

    public function getTenancyData(): ApplicationDataInterface
    {
        return $this->tenancyData;
    }

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withData(ApplicationDataInterface $tenancyData): self
    {
        $this->tenancyData = $tenancyData;
        return $this;
    }

}