<?php


namespace Plusforta\Messages\Message\Wbv;


interface TransferProcessedEvent
{

    public function getOrderId(): string;
    public function getProduct(): string;

}