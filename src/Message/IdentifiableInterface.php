<?php


namespace Plusforta\Messages\Message;


interface IdentifiableInterface
{
    public function getOrderId(): string;
    public function getProduct(): string;
}