<?php

namespace Plusforta\Messages\Message\Pdf;

use Webmozart\Assert\Assert;

class CreatePdfCommand
{
    private const TYPE_APPLICATION = 'application';
    private const TYPE_RESERVATION = 'reservation';

    private string $type;
    private string $product;
    private string $orderId;
    private array $contextData;

    private function __construct(string $product, string $orderId, string $type)
    {
        $this->product = $product;
        $this->orderId = $orderId;
        $this->type = $type;
    }


    public static function createApplicationFromOrderIdAndProduct(
        string $orderId,
        string $product
    ): self
    {
        Assert::notEmpty($product);
        Assert::notEmpty($orderId);

        return new self($product, $orderId, self::TYPE_APPLICATION);
    }

    public static function createReservationFromOrderIdAndProduct(
        string $orderId,
        string $product
    ): self
    {
        Assert::notEmpty($product);
        Assert::notEmpty($orderId);

        return new self($product, $orderId, self::TYPE_RESERVATION);
    }

    public function withContextData($contextData): self
    {
        if ($this->isApplication()) {
            $contextData = ['application' => $contextData];
        }

        if ($this->isReservation()) {
            $contextData = ['reservation' => $contextData];
        }

        $this->contextData = $contextData;
        return $this;
    }

    public function isApplication(): bool
    {
        return $this->type === self::TYPE_APPLICATION;
    }

    public function isReservation(): bool
    {
        return $this->type === self::TYPE_RESERVATION;
    }

    public function getContextData(): array
    {
        return $this->contextData;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

}