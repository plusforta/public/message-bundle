<?php


namespace Plusforta\Messages\Message\Mail;


class EmailCommandProcessedEvent
{
    private string $orderId;

    private string $product;
    private ?string $type = null;


    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }


    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function withType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function getType(): ?string
    {
        return $this->type;
    }
}