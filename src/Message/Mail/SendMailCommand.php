<?php

namespace Plusforta\Messages\Message\Mail;


use Plusforta\Messages\Mail\MailInterface;

class SendMailCommand
{
    private array $mails = [];

    private string $product;

    private string $orderId;

    private ?string $type = null;

    private function __construct(MailInterface $data, $orderId, $product)
    {
        $this->mails[] = $data;
        $this->orderId = $orderId;
        $this->product = $product;
    }

    public static function fromEmailWithOrderIdAndProduct(MailInterface $mail, string $orderId, string $product): self
    {
        return new self($mail, $orderId, $product);
    }

    public function withType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function addMail(MailInterface $mail): self
    {
        $this->mails[] = $mail;
        return $this;
    }


    public function getMails(): array
    {
        return $this->mails;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

}