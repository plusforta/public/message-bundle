<?php


namespace Plusforta\Messages\Message\Mail;

class EmailSendEvent
{
    private array $content;
    private array $meta;
    private string $email;
    private string $type;

    private string $orderId;
    private string $product;

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    private function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }


    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new self($orderId, $product);
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function getMeta(): array
    {
        return $this->meta;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function withEmail(string $email): EmailSendEvent
    {
        $this->email = $email;  
        return $this;
    }

    public function withType(string $emailType): EmailSendEvent
    {
        $this->type = $emailType;
        return $this;
    }


    public function withContent(array $content): EmailSendEvent
    {
        $this->content = $content;
        return $this;
    }

    public function withMeta(array $meta): EmailSendEvent
    {
        $this->meta = $meta;
        return $this;
    }

    public function getEmailType(): string
    {
        return $this->type;
    }

}