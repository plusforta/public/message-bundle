<?php

namespace Plusforta\Messages\Message\Storage;

use Plusforta\Messages\Dto\Storage\File;

abstract class FileEvent
{
    private string $orderId;
    private string $product;
    protected array $files = [];

    public static function fromOrderIdAndProduct(string $orderId, string $product): self
    {
        return new static($orderId, $product);
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getProduct(): string
    {
        return $this->product;
    }

    protected function __construct(string $orderId, string $product)
    {
        $this->orderId = $orderId;
        $this->product = $product;
    }

    public function addFile(File $file): FileEvent
    {
        $this->files[] = $file;
        return $this;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

}